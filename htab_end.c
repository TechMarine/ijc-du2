/**
 * TODO header
 */

#include "htab_private.h"

// vrací iterátor označující (neexistující) první záznam za koncem
htab_iterator_t htab_end(const htab_t * t){
    htab_iterator_t it;
    it.t = t;
    it.ptr = t->data[t->arr_size];
    it.idx = t->arr_size;

    return it;
}