/**
 * TODO header
 */

#include "htab_private.h"
#include <string.h>
#include <stdio.h>

// vyhledávání
// V tabulce t vyhledá záznam odpovídající řetězci key a 
//      - pokud jej nalezne, vrátí iterátor na záznam 
//      - pokud nenalezne, automaticky přidá záznam a vrátí iterátor 
// Poznámka: Dobře promyslete chování této funkce k parametru key. 
htab_iterator_t htab_lookup_add(htab_t * t, const char *key){
    
    // get array index and get its iterator
    unsigned int index = htab_hash_function(key)%t->arr_size;
    htab_iterator_t it;
    htab_iterator_t last;
    it.t = t;
    it.idx = index;
    it.ptr = t->data[index];

    // search list at index of hash function output for key
    while (it.ptr != NULL){

        if (strcmp(it.ptr->key, key) == 0){
            return it;
        }

        if (it.ptr->next == NULL) {
            last = it;
        }
        
        it.ptr = it.ptr->next;
    }

    // not found
    // create new item and return iterator
    struct htab_item *new = malloc(sizeof(struct htab_item));
    new->key = malloc(strlen(key)+1);
    strcpy(new->key, key);
    new->data = 0;
    new->next = NULL;
    if (t->data[index] != NULL) {
        last.ptr->next = new;
    } else {
        t->data[index] = new;
    }
       
    t->size++;

    it.ptr = new;
    return it;
}