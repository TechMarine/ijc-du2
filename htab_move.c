/**
 * TODO header
 */

#include "htab_private.h"

// move konstruktor: vytvoření a inicializace nové tabulky přesunem dat z tabulky t2, 
// t2 nakonec zůstane prázdná a alokovaná (tuto funkci cvičně použijte v programu podmíněným překladem #ifdef TEST)
htab_t *htab_move(size_t n, htab_t *from){
    htab_t *to = htab_init(n); 
    *to = *from;

    return to;
}