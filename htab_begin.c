/**
 * TODO header
 */

#include "htab_private.h"

// vrací iterátor označující první záznam
htab_iterator_t htab_begin(const htab_t * t){
    htab_iterator_t it;
    it.ptr = t->data[0];
    it.idx = 0;
    it.t = t;

    for(int i = 0; i < t->arr_size; i++)
    {
        if (t->data[i] != NULL) {
            it.ptr = t->data[i];
            it.idx = i;
            return it;
        }
        
    }
    
    return it;
}