/**
 * TODO header
 */

#include "htab_private.h"

// destruktor: zrušení tabulky (volá htab_clear())
void htab_free(htab_t * t){
    htab_clear(t);

    free(t);
    t = NULL;
}