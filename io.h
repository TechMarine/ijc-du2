/**
 * TODO header
 */

#ifndef __IO_H__ 
#define __IO_H__ 

#include <stdio.h>
#include <ctype.h>
#include <string.h>

int get_word(char *s, int max, FILE *f);

#endif //__IO_H__