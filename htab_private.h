/**
 * TODO header
 */

#ifndef __HTABLE_PRIVATE_H__ 
#define __HTABLE_PRIVATE_H__ 

#include "htab.h"
#include <stdlib.h>

struct htab {
    long size;
    long arr_size;
    struct htab_item *data[];
};


struct htab_item {
    char *key;
    int data;
    struct htab_item *next;
};

#endif //__HTABLE_PRIVATE_H__ 