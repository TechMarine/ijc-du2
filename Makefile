#
#
#

CC= gcc
CFLAGS= -std=c99 -O2 -pedantic -Wall

all: tail tail2 wordcount wordcount-dynamic

tail: tail.c
	$(CC) $(CFLAGS) tail.c -o tail

tail2: tail2.cc
	g++ -std=c++11 tail2.cc -o tail2

wordcount: wordcount.c io.o libhtab.a
	$(CC) $(CFLAGS) -c wordcount.c -o wordcount.o
	gcc wordcount.o io.o libhtab.a -o wordcount

wordcount-dynamic: wordcount.c io.o libhtab.so
	gcc -fPIC wordcount.c io.o -L. -lhtab -o wordcount-dynamic

run_dynamic: wordcount-dynamic
	LD_LIBRARY_PATH="." ./wordcount-dynamic

io.o: io.c io.h
	$(CC) $(CFLAGS) -c io.c io.h

libhtab.a: htab.h htab_private.h htab_hash_function.o htab_init.o htab_move.o htab_size.o htab_bucket_count.o htab_lookup_add.o htab_begin.o htab_end.o htab_iterator_next.o htab_iterator_get_key.o htab_iterator_get_value.o htab_iterator_set_value.o htab_clear.o htab_free.o
	ar rcs libhtab.a htab.h htab_private.h htab_hash_function.o htab_init.o htab_move.o htab_size.o htab_bucket_count.o htab_lookup_add.o htab_begin.o htab_end.o htab_iterator_next.o htab_iterator_get_key.o htab_iterator_get_value.o htab_iterator_set_value.o htab_clear.o htab_free.o

libhtab.so: htab.h htab_private.h htab_hash_function_d.o htab_init_d.o htab_move_d.o htab_size_d.o htab_bucket_count_d.o htab_lookup_add_d.o htab_begin_d.o htab_end_d.o htab_iterator_next_d.o htab_iterator_get_key_d.o htab_iterator_get_value_d.o htab_iterator_set_value_d.o htab_clear_d.o htab_free_d.o
	$(CC) $(CFLAGS) -shared -fPIC htab.h htab_private.h htab_hash_function_d.o htab_init_d.o htab_move_d.o htab_size_d.o htab_bucket_count_d.o htab_lookup_add_d.o htab_begin_d.o htab_end_d.o htab_iterator_next_d.o htab_iterator_get_key_d.o htab_iterator_get_value_d.o htab_iterator_set_value_d.o htab_clear_d.o htab_free_d.o -o libhtab.so

# TODO normal .o files without -fpic for static lib
htab_hash_function.o: htab_hash_function.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_hash_function.c -o htab_hash_function.o

htab_init.o: htab_init.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_init.c -o htab_init.o

htab_move.o: htab_move.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_move.c -o htab_move.o

htab_size.o: htab_size.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_size.c -o htab_size.o

htab_bucket_count.o: htab_bucket_count.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_bucket_count.c -o htab_bucket_count.o

htab_lookup_add.o: htab_lookup_add.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_lookup_add.c -o htab_lookup_add.o

htab_begin.o: htab_begin.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_begin.c -o htab_begin.o

htab_end.o: htab_end.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_end.c -o htab_end.o

htab_iterator_next.o: htab_iterator_next.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_iterator_next.c -o htab_iterator_next.o

htab_iterator_get_key.o: htab_iterator_get_key.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_iterator_get_key.c -o htab_iterator_get_key.o

htab_iterator_get_value.o: htab_iterator_get_value.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_iterator_get_value.c -o htab_iterator_get_value.o

htab_iterator_set_value.o: htab_iterator_set_value.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_iterator_set_value.c -o htab_iterator_set_value.o

htab_clear.o: htab_clear.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_clear.c -o htab_clear.o

htab_free.o: htab_free.c htab_private.h htab.h
	$(CC) $(CFLAGS) -c htab_free.c -o htab_free.o


# object files for shared library (with fpic)
htab_hash_function_d.o: htab_hash_function.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_hash_function.c -o htab_hash_function_d.o

htab_init_d.o: htab_init.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_init.c -o htab_init_d.o

htab_move_d.o: htab_move.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_move.c -o htab_move_d.o

htab_size_d.o: htab_size.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_size.c -o htab_size_d.o

htab_bucket_count_d.o: htab_bucket_count.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_bucket_count.c -o htab_bucket_count_d.o

htab_lookup_add_d.o: htab_lookup_add.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_lookup_add.c -o htab_lookup_add_d.o

htab_begin_d.o: htab_begin.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_begin.c -o htab_begin_d.o

htab_end_d.o: htab_end.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_end.c -o htab_end_d.o

htab_iterator_next_d.o: htab_iterator_next.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_iterator_next.c -o htab_iterator_next_d.o

htab_iterator_get_key_d.o: htab_iterator_get_key.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_iterator_get_key.c -o htab_iterator_get_key_d.o

htab_iterator_get_value_d.o: htab_iterator_get_value.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_iterator_get_value.c -o htab_iterator_get_value_d.o

htab_iterator_set_value_d.o: htab_iterator_set_value.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_iterator_set_value.c -o htab_iterator_set_value_d.o

htab_clear_d.o: htab_clear.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_clear.c -o htab_clear_d.o

htab_free_d.o: htab_free.c htab_private.h htab.h
	$(CC) $(CFLAGS) -fPIC -c htab_free.c -o htab_free_d.o
