/**
 * TODO header
 */

#include "htab_private.h"

// zrušení všech položek, tabulka zůstane prázdná
void htab_clear(htab_t * t){
    for(size_t i = 0; i < t->arr_size; i++)
    {
        if (t->data[i] != NULL) {
            while(t->data[i]->next != NULL){
                /* code */
                htab_iterator_t it;
                it.idx = i;
                it.ptr = t->data[i];
                it.t = t;

                while(it.ptr->next->next != NULL)
                    it.ptr = it.ptr->next;

                free(it.ptr->next->key);
                free(it.ptr->next);
                it.ptr->next = NULL;
            }
            free(t->data[i]);
            t->data[i] = NULL;

        }
        
    }
    
}