/**
 * TODO header
 */

#include "io.h"

// čte jedno slovo ze souboru f do zadaného pole znaků a vrátí délku slova (z delších slov načte prvních max-1 znaků, a zbytek přeskočí).
// Funkce vrací EOF, pokud je konec souboru.
// Poznámka: Slovo je souvislá posloupnost znaků oddělená isspace znaky.
int get_word(char *s, int max, FILE *f){
    int c, count = 0;
    while (!isspace(c = fgetc(f))){
        if (c == EOF) {
            if (count > 0 && s[count-1] != '\0' ) {
                s[count] = '\0';
            }
            
            return EOF;
        }

        if (count < max){
            s[count] = c;
            count++;
        } 
    }
    s[count] = '\0';
    return count;
}