/**
 * TODO header
 */

#include "htab.h"
#include "io.h"

// TODO wordcount
int main() {
    
    char word[128];
    htab_t *t = htab_init(100);

    if (t == NULL) {
        fprintf(stderr, "chyba vytvoreni hashovaci tabulky\n");
        return 1;
    }

    while(get_word(word, 128, stdin) != EOF){
        htab_iterator_t it = htab_lookup_add(t, word);
        htab_iterator_set_value(it, htab_iterator_get_value(it) + 1);
    }

    htab_iterator_t it = htab_begin(t);

    for(int i = 0; i < htab_size(t) ; i++)
    {
        printf("%s\t%d\n", htab_iterator_get_key(it), htab_iterator_get_value(it));
        it = htab_iterator_next(it);
    }
    
    htab_free(t);

    return 0;
}