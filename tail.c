/*  tail.c
*   TODO header
*
*
*
***********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int n = 10; // number of lines to print (defaultly 10)
    char *fileName = NULL;
    FILE *file = NULL;

    // get arg with number of lines, if not specified use 10
    // check if n > 0
    if ((argc == 4 || argc == 3) && strcmp(argv[1], "-n") == 0)
    {
        if (sscanf (argv[2], "%i", &n) != 1) 
            fprintf(stderr, "error - value of argument -n is not an integer\n");
            
        if (n <= 0)
        {
            fprintf(stderr, "error - wrong number of lines, must be greater than 0\n");  
            return 1;
        }
    }
    else if (argc != 1 && argc != 2)
    {
        fprintf(stderr, "tail.c run with wrong arguments\n");
        return 1;
    }

    // if argc is 4 or 2 read from file
    if (argc == 4) 
        fileName = argv[3];
    else if (argc == 2)
        fileName = argv[1];

    // if argc is 3 || 1 input from file is in stdin (file == null <-- test later);


    // allocate buffer for lines
    char line[1024] = {0,};
    char ** buffer = malloc(sizeof(char *) * n);
    for(int i = 0; i < n; i++)
    {
        buffer[i] = malloc(sizeof(line));
    }
    
    int counter = 0;

    // load lines from file and save them to buffer until eof
    if (fileName != NULL) {
        file = fopen(fileName, "r");

        while(fgets(buffer[counter], 1024, file) != NULL)
        {
            // TODO check if buffer length is 1023 and does not end with '\n' then getc until '\n' is found then load next line (if this happened for the first time print warning to stderr)

            counter++;
            if (counter >= n) 
                counter = 0;
        }

        fclose(file);
    }
    else
    {
        // load lines from stdin and save them to buffer until eof
        while(fgets(buffer[counter], 1024, stdin) != NULL)
        {
                counter++;
                if (counter >= n) 
                    counter = 0;                    
        }
    }      

    
    // print buffer from oldest to newest
    int j = counter;
    for(int i = 0; i < n; i++)
    {
        printf("%s", buffer[j]);

        j++;
        if (j >= n) 
        {
            j = 0;
        }
                  
    }
    
    // free buffer and exit
    for(int i = 0; i < n; i++)
        free(buffer[i]);
    free(buffer);

    return 0;
}
