/**
 * TODO header
 */

#include "htab_private.h"


extern bool htab_iterator_equal(htab_iterator_t, htab_iterator_t);

// posun iterátoru na další záznam v tabulce (nebo na htab_end(t))
htab_iterator_t htab_iterator_next(htab_iterator_t it){
    
    // if iterator is null find first non null list
    if (it.ptr == NULL) {
        for(int i = it.idx; i < it.t->arr_size; i++)
        {
            if (it.t->data[i] != NULL) {
                it.ptr = it.t->data[i];
                it.idx = i;
            }
            
        }
        
    }
    

    if (it.ptr->next != NULL){ // jump to next in current list
        it.ptr = it.ptr->next;
        return it;
    } else if (it.idx + 1 <= it.t->arr_size){ //jump to next list
        do {
            it.idx++;
        } while (it.t->data[it.idx] == NULL && !htab_iterator_equal(it, htab_end(it.t)));
            
        if (!htab_iterator_equal(it, htab_end(it.t)))
            it.ptr = it.t->data[it.idx]; // jump to next not null list
        else
            it = htab_end(it.t); // is at end
        
        return it; 
    }
    return it; // already at htab_end
}