/*
*
*
*
*
*/

#include <iostream>
#include <queue>
#include <string>
#include <fstream>

using namespace std;

int main(int argc, char *argv[])
{

    ios::sync_with_stdio(false);
    
    int n = 10;
    ifstream file;
    string fileName;

    if ((argc == 4 || argc == 3) && argv[1] == string("-n")) 
    {
        n = stoi(argv[2]);

        if (n <= 0)
        {
            fprintf(stderr, "error - wrong number of lines, must be greater than 0\n"); 
            return 1;
        }
    }
    else if (argc != 1 && argc != 2)
    {
        cerr << "tail.c run with wrong arguments" << endl;
        return 1;
    }

    if (argc == 4)
        fileName = argv[3];
    else if (argc == 2)
        fileName = argv[1];
    
    
    // load lines to queue
    queue<string> qStrings;
    string line;

    
    if (!fileName.empty()) {
        // load lines from file
        file.open(fileName);
        while(!file.eof())
        {      
            getline(file, line); 
            if (qStrings.size() > n) 
                qStrings.pop();

            qStrings.push(line);
        }
        file.close();
    }
    else
    {
        // load lines from cin (stdin)
        while(!cin.eof())
        {      
            getline(cin, line); 
            if (qStrings.size() > n) 
                qStrings.pop();

            qStrings.push(line);
        }
    }
    


    

    // std::getline(istream, string)
    int size = qStrings.size();
    for(int i = 0; i < size - 1; i++)
    {
        cout << qStrings.front() << endl;
        qStrings.pop();
    }
   

    return 0;
}
