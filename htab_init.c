/**
 * TODO header
 */

#include "htab_private.h"

// initialize htab with n = number of elements (.arr_size)
htab_t *htab_init(size_t n){
    htab_t *t = malloc(sizeof(struct htab_item *) * n + sizeof(htab_t));
    t->arr_size = n;
    t->size = 0;

    for(size_t i = 0; i < n; i++)
    {
        t->data[i] = NULL;
    }
    

    return t;
}